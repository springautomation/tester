/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exercise.spring.context;

import com.exercise.spring.config.Album;
import com.exercise.spring.config.AppConfig;
import com.exercise.spring.model.Photo;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author darek
 */
public class ConsumeRESTFulService {

    private AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

    private Album api = applicationContext.getBean(Album.class);
    private Album json = applicationContext.getBean(Album.class);

    private List<Photo> apiPhotos;
    private List<Photo> jsonPhotos;

    public void ConsumingRestService() {
        apiPhotos = api.getAllApiPhotos();
        jsonPhotos = json.getAllJsonPhotos();

        applicationContext.close();
    }

    public List<Photo> getApiAlbums() {
        return apiPhotos;
    }

    public List<Photo> getJsonAlbums() {
        return jsonPhotos;
    }
}
