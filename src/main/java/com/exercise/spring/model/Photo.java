package com.exercise.spring.model;

public class Photo {

    private int albumId;
    private int id;
    private String title;
    private String url;
    private String thumbnailUrl;

    public Photo() {
    }

    public int getAlbumId() {
        return this.albumId;
    }

    public int getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getUrl() {
        return this.url;
    }

    public String getThumbnailUrl() {
        return this.thumbnailUrl;
    }
    
    public void setAlbumId(int albumId){
        this.albumId = albumId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public void setUrl(String url) {
         this.url = url;
    }

    public void getThumbnailUrl(String thumbnailUrl) {
         this.thumbnailUrl = thumbnailUrl;
    }
    @Override
    public String toString() {
        return  "albumId: " + albumId 
                + "\nid: " + id 
                + "\ntitle: " + title 
                + "\nurl: " + url 
                + "\nthumbnailUrl: " + thumbnailUrl 
                ;
    }
}