package com.exercise.spring.config;

import com.exercise.spring.constants.Constants;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.exercise.spring.model.Photo;

@Service
public class AlbumImpl implements Album {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    List<Photo> JsonAlbums;

    public List<Photo> getAllApiPhotos() {
        ResponseEntity<Photo[]> response = restTemplate.getForEntity(Constants.endpointPhotos, Photo[].class);
        return Arrays.asList(response.getBody());
    }

    public Photo getApiPhotoById(int id) {
        ResponseEntity<Photo> response = restTemplate.getForEntity(Constants.endpointPhotos + id, Photo.class);
        return response.getBody();
    }

    public List<Photo> getAllJsonPhotos() {

        return JsonAlbums;
    }

    public Photo getJsonPhotoById(int id) {
        Photo photo = JsonAlbums.get(id);
        
        return photo;
    }

}
