package com.exercise.spring.config;

import java.util.List;

import com.exercise.spring.model.Photo;

public interface Album {

    List<Photo> getAllApiPhotos();

    Photo getApiPhotoById(int id);

    List<Photo> getAllJsonPhotos();
    
    Photo getJsonPhotoById(int id);

}
