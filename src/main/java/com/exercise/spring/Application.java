package com.exercise.spring;

import com.exercise.spring.test.TestRESTFulService;
import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {

        TestRESTFulService restClient = new TestRESTFulService();
        restClient.testRunning();
        restClient.validatingContext();
    }

}
