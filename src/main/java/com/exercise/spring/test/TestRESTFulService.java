/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exercise.spring.test;

import com.exercise.spring.context.ConsumeRESTFulService;
import java.io.IOException;

public class TestRESTFulService {

    private ConsumeRESTFulService appContext = new ConsumeRESTFulService();
    
    private String jsonAlbum;
    private String apiAlbum;
    
    private boolean result;

    public void testRunning() throws IOException {
       appContext.ConsumingRestService();
    }

    public void validatingContext() {
        for (int album = 0; album < appContext.getJsonAlbums().size(); album++) {
            jsonAlbum = appContext.getJsonAlbums().get(album).toString();
            apiAlbum = appContext.getApiAlbums().get(album).toString();

            result = apiAlbum.equals(jsonAlbum);
            validationStatus(result);
        }
    }

    public void validationStatus(boolean result) {       
        if (result) {
            System.out.println("\nValidation result: " + result + "\n" + apiAlbum);
        } else {
            System.out.println("\nValidation result: " + result + "\n" + apiAlbum);
        }
    }
}
